const Constants = {
  REELS: 3,
  REEL_SECTORS: 4,
  NUM_SYMBOLS: 2,
  REEL_RERUNS: 10,
  SYMBOL_HEIGHT: 200,
  SYMBOL_PAD: 20,
  SPIN_SPEED: 1,
};

export default Constants;
