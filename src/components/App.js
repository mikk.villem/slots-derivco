import React from "react";
/* import Constants from "../config/Constants"; */
import ReelSet from "./ReelSet";
import "../styles/App.scss";

/* function spin() {
  console.log("spin event");
} */

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.reelSet = null;
  }

  render() {
    return (
      <>
        <div className="screen">
          <ReelSet
            ref={(ref) => {
              this.reelSet = ref;
            }}
          />
        </div>
        <button
          className="spin-button"
          onClick={() => /* console.log("spin") */ this.reelSet.spin()}>
          <p>SPIN</p>
        </button>
      </>
    );
  }
}
