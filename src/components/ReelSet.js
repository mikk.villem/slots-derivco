import React from "react";
import Reel from "./Reel";
import Constants from "../config/Constants";

export default class ReelSet extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      reels: Array.from({ length: Constants.REELS }, (_, index) => index),
      width: "200px",
      height: "200px",
    };
    this.reels = [];
    this.reelsInMotion = null;
    this.spinResults = [];
  }

  //Random number between and including Min/Max
  randomBetween = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1) + min);
  };

  spin = () => {
    this.reelsInMotion = Constants.REELS;
    for (let i = 0; i < Constants.REELS; i++) {
      let min =
        (Constants.REEL_RERUNS - 7) * Array.from(this.reels[i].symbols).length; // 3 * 5 = 15
      let max =
        (Constants.REEL_RERUNS - 7) * Array.from(this.reels[i].symbols).length; // 3 * 5 = 15

      this.reels[i].scrollByOffset(
        this.randomBetween(min, max),
        (reelIdx, results) => {
          this.reelsInMotion -= 1;
          this.spinResults[reelIdx] = results;
        }
      ); // spin reels down between 15 an 15 symbols
    }
  };

  renderReels = () => {
    return (
      <>
        {this.state.reels.map((reel, idx) => {
          return (
            <Reel
              key={idx}
              index={idx}
              width={this.state.width}
              height={this.state.height}
              ref={(ref) => {
                this.reels[idx] = ref;
              }}
            />
          );
        })}
      </>
    );
  };

  render() {
    return <>{this.renderReels()}</>;
  }
}

/* const ReelSet = () => {
  const [reels] = useState(
    Array.from({ length: Constants.REELS }, (_, index) => index)
  );

  const reelRefs = useRef(
    Array.from({ length: Constants.REELS }, () => React.createRef())
  );

  const spin = (idx) => {
    reelRefs[idx].scrollByOffset(10);
  };

  return (
    <>
      {reels.map((reel, idx) => {
        return (
          <Reel
            key={idx}
            index={idx}
            width="200px"
            height="200px"
            ref={reelRefs[idx]}
            onClick={(idx) => spin(idx)}
          />
        );
      })}
    </>
  );
};

export default ReelSet;
 */
